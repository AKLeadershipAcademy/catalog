# How to get Involved in Writing Products for the Catalog

## Signing Up for GitLab

1. [Register for GitLab.com](https://gitlab.com/users/sign_in#register-pane)
    1. Full Name or at least your last name and first initial so that I can recognize your request later on.
    2. Make a user name.
    3. Provide your email address probably best to use your private email so you can also register from home.
    4. Confirm email address.
    5. Make a password at least 8 characters and dont forget it.
    6. Accept terms.
    7. Prove that you are not a robot.
    8. Press the register button
    <img src="https://gitlab.com/AKLeadershipAcademy/catalog/raw/master/source/_static/assets/images/GitLab_Register.png" alt="GitLab.com Registration Page" width="500">
<br>
<br>
2. Open your email account and confirm the account by validating your email address. Simply click the link in the email.<br>
    <img src="https://gitlab.com/AKLeadershipAcademy/catalog/raw/master/source/_static/assets/images/GitLab_Confirm_Email.png" alt="GitLab.com Confirmation Email" width="500">
<br>
<br>

## Join Alaska Leadership Academy (AKLeadershipAcademy) on Git Lab

1. [Sign in to GitLab.com](https://gitlab.com/users/sign_in)<br>
    <img src="https://gitlab.com/AKLeadershipAcademy/catalog/raw/master/source/_static/assets/images/GitLab_Sign_In.png" alt="GitLab.com Confirmation Email" width="500">
<br>
<br>
2. Click "Groups" then [Explore Groups](https://gitlab.com/explore/groups).<br>
    <img src="https://gitlab.com/AKLeadershipAcademy/catalog/raw/master/source/_static/assets/images/GitLab_Explore_Groups.png" alt="GitLab.com Explore Groups" width="500">
<br>
<br>
3. Search for and select "AKLeadershipAcademy".<br>
    <img src="https://gitlab.com/AKLeadershipAcademy/catalog/raw/master/source/_static/assets/images/Search_AKLeadershipAcademy_Group.png" alt="Search for AKLeadershipAcademy" width="500">
<br>
<br>
4. Request Access to AKLeadershipAcademy.<br>
    <img src="https://gitlab.com/AKLeadershipAcademy/catalog/raw/master/source/_static/assets/images/AKLeadershipAcademy_Request_Access.png" alt="Search for AKLeadershipAcademy" width="500">
<br>
<br>
5. Wait for Access to be Granted.<br>
    <img src="https://gitlab.com/AKLeadershipAcademy/catalog/raw/master/source/_static/assets/images/AKLeadershipAcademy_Requested_Access.png" alt="Search for AKLeadershipAcademy" width="500">
